#!/bin/bash

set -euo pipefail

NAMESPACE=nexus
CHART=nexus
ARGOCD_HOST=argocd.craigmiller160.us
ARGOCD_REPO_PATH="../craigmiller160-projects-argocd"
ARGOCD_LOCAL_PROJECT_PATH="./namespaces/$NAMESPACE/$CHART/project.yaml"
ARGOCD_PROJECT_PATH="$ARGOCD_REPO_PATH/$ARGOCD_LOCAL_PROJECT_PATH"

deploy_argocd() {
  echo "Preparing ArgoCD repository"
  (
    cd "$ARGOCD_REPO_PATH"
    git reset 1>/dev/null
    git pull 1>/dev/null
  )

  echo "Generating helm template"
  helm template \
    -n "$NAMESPACE" \
    "$CHART" \
    ./deploy/chart \
    --values ./deploy/chart/values.yaml \
    > "$ARGOCD_PROJECT_PATH"

  repo_changes=$(
    cd "$ARGOCD_REPO_PATH"
    git status "$ARGOCD_LOCAL_PROJECT_PATH" --porcelain
  )

  if [ "$repo_changes" != "" ]; then
    echo "Project has no changes to deploy"
    echo "Committing & pushing changes"
    (
      cd "$ARGOCD_REPO_PATH"
      git add "$ARGOCD_LOCAL_PROJECT_PATH" 1>/dev/null
      git commit -m "Updating $CHART" 1>/dev/null
      git push 1>/dev/null
    )
  fi

  echo "Sync-ing with ArgoCD"
  argocd login \
    "$ARGOCD_HOST" \
    --grpc-web \
    --username "$ARGOCD_USERNAME" \
    --password "$ARGOCD_PASSWORD"

  argocd app \
    sync \
    "$CHART" \
    --grpc-web

  argocd app \
    wait \
    "$CHART" \
    --timeout 300 \
    --grpc-web
}

run_terraform() {
  echo "Running terraform script"
  (
    cd ./deploy/terraform &&
      terraform apply \
        -var=onepassword_token="$ONEPASSWORD_TOKEN" \
        -var=env=prod
  )
}

deploy_argocd
run_terraform